# serial_echo.py - Alexander Hiam - 4/15/12
# 
# Prints all incoming data on Serial2 and echos it back.
# 
# Serial2 TX = pin 21 on P9 header
# Serial2 RX = pin 22 on P9 header
# 
# This example is in the public domain

from bbio import *
import threading

import exampleftp2
import File_Test
import time
import captura_img
import sys
import shutil
import os

data1 = 0
pos = 0
x = 0
aux = [] 
b = 0
j = 0
k=1

FLAG_archivo = True
Dif_Tem = 0
Cont = 0
Inicio = 0


i = 0
Recepcion = []

DatoXY = []

filename = ''
filename1 = ''
File = ''
direccion = ''
direccion_Img = ''
	 
## Adicion de dato desde puerto serial
def addDato(Dato, auxCop):

     global x, b
     Aux_Conv = 0 	
     
     #Aux_Con = int(Dato) - 0x64
     auxCop.insert(x,Dato.encode("hex")) 
     x = x+1
     if(x == 19):
       x = 0	
       b = 1		
               
def idenTrama(trama):
     global Recepcion
     global j, k, i	
     i = 0   	
     print "len = ", len(trama)
     for i in range(len(trama)):
	  if str(trama[i])=='7e':
	     print "i -->", i
	     while(j < 19):	       	
	       Recepcion.insert(j,trama[(j+i)])
               j =j+1 

		#k=k+1
		#if (k==10):
		#k=0


def setup():
   #global filename
  # Start Serial2 at 9600 baud:
  Serial2.begin(9600)
 # lock=threading.lock()


def loop():
  
  global b , j , i	
  global aux
  global Recepcion
  global filename, File, direccion, direccion_Img
  global FLAG_archivo, Cont, captura_img, Inicio
  Hay_dato = False
  Dato_Int = 0	
  

  if (Serial2.available()): 
	if (FLAG_archivo == True):
           name="datos"
	   name1 = "Img"	

	   hora=time.strftime("%a%d%b%Y_%H_%M_%S")
	   filename = name + hora + str('.txt')
	   filename_Img = name1 + hora + str('.jpg')
	   carpeta = 'Prueba' + str(Cont)

	   File = '/media/6BFE-9C21/' + carpeta

	   direccion = File + '/' + filename  #directorio enviado para ingresar dato al TXT
	   direccion_Img = File + '/' + filename_Img
	   #print direccion_Img	

	   answer= os.path.isdir('/media/6BFE-9C21/' + carpeta)
	   if answer== True:
	        shutil.rmtree('/media/6BFE-9C21/' + carpeta)

           File_Test.crearCarpeta(R'/media/6BFE-9C21/' + carpeta)

   	   File_Test.crearTXT(File, filename)
	   File_Test.crearTXT('/media/6BFE-9C21/', 'fecha1.txt')
           FLAG_archivo= False
	   Cont +=1	
	

  #Serial2.write("a")
  if (Serial2.available()):
    Inicio=time.time()
    # There's incoming data
    Hay_dato = True

    while(Serial2.available()):
  
        data = Serial2.read()
	

       # data1 = data.encode("hex")
       	
	addDato(data,aux)

    if(b == 1):	
      idenTrama(aux)
      
      if (Recepcion == []):
          print "envia vacio\n" 
	  File_Test.grabarTXT(direccion,"0")
	  File_Test.grabarTXT('/media/6BFE-9C21/fecha1.txt', time.strftime("%d/%m/%y %H:%M:%S"))

      else:
          print "enviando  ", Recepcion[17], "\n"	
          Dato_Int = int(Recepcion[17],16)-100
	  File_Test.grabarTXT(direccion, str(Dato_Int)) #Recepcion[17]
	  File_Test.grabarTXT('/media/6BFE-9C21/fecha1.txt', time.strftime("%d/%m/%y %H:%M:%S"))
	  # COLOCAR FUNCION PARA ENVIAR DATO A WEB 
      aux=[]
      j = 0 

    b = 0	 	
    i = 0	
    Recepcion = []
    Trama = []
  Fin = time.time()
  Dif_Tem = Fin-Inicio
  Dif_Tem = int(Dif_Tem)
  if(Dif_Tem == 15):
        
 	FLAG_archivo = True

	shutil.copyfile(direccion, '/media/6BFE-9C21/datos1.txt')  

	print "Entra Hilo Captura"
	#lock.acquire()
	# Hilo para captura de imagen
        thread = threading.Thread(target = captura_img.get_Image, args = (direccion_Img,))
	thread.daemon = True
	thread.start()
	thread.join()
        print "Sale Hilo Captura" 

	print "Entra Hilo FTP_Img"
	# Hilo para subir archivos a ftp
	thread_img = threading.Thread(target = exampleftp2.envio_server_img, args = ('/media/6BFE-9C21/derrumbe1.jpg',))
	thread_img.daemon = True
	thread_img.start()
	thread_img.join()
	#lock.release()
        print "Sale Hilo FTP_Img" 

	print "Entra Hilo FTP_Img_back up"
	thread_img_bk = threading.Thread(target = exampleftp2.envio_server, args = (direccion_Img,))
	thread_img_bk.daemon = True
	thread_img_bk.start()
	thread_img_bk.join()
	#lock.release()
        print "Sale Hilo FTP_Img_back up" 

	print "Entra Hilo FTP_TXT"
	# Hilo para subir archivos a ftp
	thread_txt = threading.Thread(target = exampleftp2.envio_server, args = ('/media/6BFE-9C21/datos1.txt',))
	thread_txt.daemon = True
	thread_txt.start()
	thread_txt.join()
        print "Sale Hilo FTP_TXT"

	print "Entra Hilo FTP_TXT"
	# Hilo para subir archivos a ftp
	thread_txt = threading.Thread(target = exampleftp2.envio_server, args = ('/media/6BFE-9C21/fecha1.txt',))
	thread_txt.daemon = True
	thread_txt.start()
	thread_txt.join()
        print "Sale Hilo FTP_TXT"	

	print "Entra Hilo FTP_TXT back up"
	thread_txt_bk = threading.Thread(target = exampleftp2.envio_server_backup, args = (direccion,))
	thread_txt_bk.daemon = True
	thread_txt_bk.start()
	thread_txt_bk.join()
	#lock.release()
        print "Sale Hilo FTP_TXT back up"

    #if (b == 0):

    #Serial2.write(data)


run(setup,loop)














  	
	
   	
    	


