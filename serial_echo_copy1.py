# serial_echo.py - Alexander Hiam - 4/15/12
# 
# Prints all incoming data on Serial2 and echos it back.
# 
# Serial2 TX = pin 21 on P9 header
# Serial2 RX = pin 22 on P9 header
# 
# This example is in the public domain

from bbio import *

Recepcion = []
i = 1
data = ''
Aux = 0


def addDato(Dato):
   
   global i
   global Recepcion
   global data
   global Aux
		   

   Aux = Dato.encode("hex")
   if(Aux == '7e'):	
	Recepcion.insert(0,Aux)
	while(i<17):
	   data = Serial2.read()
	   Aux = data.encode("hex")
	   Recepcion.insert(i,Aux)
	   i += 1
	print "Vector ",Recepcion,'\n'
	i = 1        
	Recepcion = []
	

def setup():
  # Start Serial2 at 9600 baud:
  Serial2.begin(9600)


def loop():
  global i
  global Recepcion
  global data
  global Aux

  if (Serial2.available()):

    while(Serial2.available()):
        data = Serial2.read()
	addDato(data)
	






run(setup,loop)