// Test to check the OpenCV version
// Build on Linux with:
// g++ test_1.cpp -o test_1 -lopencv_core

#include <opencv2/opencv.hpp>
#include <iostream>

int main() {
	std::cout << "Hello, OpenCV version "<< CV_VERSION << std::endl;
	return 0;
}
