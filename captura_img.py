import cv2.cv as cv
import cv2
import time
import numpy as np
import os

def get_Image(Dir): 

	video=cv2.VideoCapture(0) 

	video.set(3,320)
	video.set(4,240)
	
	time.sleep(2)   #tiempo que espera para guardar imagen
	
	_, frame=video.read()
	
	gris = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

	cv2.imwrite(Dir, frame)
	cv2.imwrite("/media/DABC-9FA5/derrumbe1.jpg", frame)
	
	video.release()
	cv2.destroyAllWindows()
