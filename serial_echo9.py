# serial_echo.py - Alexander Hiam - 4/15/12
# 
# Prints all incoming data on Serial2 and echos it back.
# 
# Serial2 TX = pin 21 on P9 header
# Serial2 RX = pin 22 on P9 header
# 
# This example is in the public domain

from bbio import *
import threading

import exampleftp2
import Read_Serial
import File_Test
import time
import captura_img
import sys
import shutil
import os
import time

pos = 0
b = 0
j = 0
k=1

FLAG_archivo = True

flag_IoT = False
Dif_Tem = 0
Cont = 0
Inicio = 0


i = 0

filename = ''
filename1 = ''
File = ''
direccion = ''
direccion_Img = ''	 
             
def setup():
  Serial2.begin(9600)
  pinMode(GPIO2_4, OUTPUT)
  digitalWrite(GPIO2_4, HIGH)
 
 # lock=threading.lock()

def loop():
  
  global b , j , i	
  global aux
  global Recepcion
  global filename, File, direccion, direccion_Img
  global FLAG_archivo, Cont, captura_img, Inicio
  global flag_IoT 
  Hay_dato = False
  Dato_Int = 0	
  Dato1 = 0
  b_i = 0
  

  if (Serial2.available()): 
	if (FLAG_archivo == True):
           name="dato"
	   name1 = "Img"	

	   hora=time.strftime("%a%d%b%Y_%H_%M_%S")
	   filename = name + hora + str('.txt')
	   filename_Img = name1 + hora + str('.jpg')
	   carpeta = 'Prueba' + str(Cont)

	   File = '/media/MEMORIA_VIB/' + carpeta

	   direccion = File + '/' + filename  #directorio enviado para ingresar dato al TXT
	   direccion_Img = File + '/' + filename_Img
	   #print direccion_Img	

	   answer= os.path.isdir('/media/MEMORIA_VIB/' + carpeta)
	   if answer== True:
	        shutil.rmtree('/media/MEMORIA_VIB/' + carpeta)

           File_Test.crearCarpeta(R'/media/MEMORIA_VIB/' + carpeta)

   	   File_Test.crearTXT(File, filename)
	   File_Test.crearTXT('/media/MEMORIA_VIB/', 'fecha.txt')
           FLAG_archivo= False
	   Cont +=1	
	

  #Serial2.write("a")
  if (Serial2.available()):
    Inicio=time.time()
    # There's incoming data
    Hay_dato = True

    while(Serial2.available()):
	  lista=[0,0]
          lista=Read_Serial.readSerial()
	  Dato_Int = lista[0]-100
          #print "Dato 1: ", Dato_Int
          if(lista[1] == 17):
             print "Dato  ", Dato_Int
	     File_Test.grabarTXT(direccion, str(Dato_Int))
	     File_Test.grabarTXT('/media/MEMORIA_VIB/fecha.txt', time.strftime("%d/%m/%y %H:%M:%S"))
	     if (Dato_Int > 14):
	         flag_IoT = True
  	    
  Fin = time.time()
  Dif_Tem = Fin-Inicio
  Dif_Tem = int(Dif_Tem)
  if(Dif_Tem == 15):
        
 	FLAG_archivo = True
	shutil.copyfile(direccion, '/media/MEMORIA_VIB/dato.txt')  
        
	if (flag_IoT == True):
		z=0
		for z in range(5): 
 		     digitalWrite(GPIO2_4, LOW)
		     delay(2)
		     digitalWrite(GPIO2_4, HIGH)
		     delay(2)
		
		print 'ENVIO ALARMA!!' 
		flag_IoT = False     

	#lock.acquire()
	# Hilo para captura de imagen
        thread = threading.Thread(target = captura_img.get_Image, args = (direccion_Img,))
	thread.daemon = True
	thread.start()
	thread.join()
        
	# Hilo para subir archivos a ftp
	thread_img = threading.Thread(target = exampleftp2.envio_server_img, args = ('/media/MEMORIA_VIB/derrumbe1.jpg',))
	thread_img.daemon = True
	thread_img.start()
	thread_img.join()
	#lock.release()
        
	thread_img_bk = threading.Thread(target = exampleftp2.envio_server, args = (direccion_Img,))
	thread_img_bk.daemon = True
	thread_img_bk.start()

	thread_img_bk.join()
	#lock.release()

	# Hilo para subir archivos a ftp
	thread_txt = threading.Thread(target = exampleftp2.envio_server, args = ('/media/MEMORIA_VIB/dato.txt',))
	thread_txt.daemon = True
	thread_txt.start()
	thread_txt.join()
    
	# Hilo para subir archivos a ftp
	thread_txt = threading.Thread(target = exampleftp2.envio_server, args = ('/media/MEMORIA_VIB/fecha.txt',))
	thread_txt.daemon = True
	thread_txt.start()
	thread_txt.join()
        
	thread_txt_bk = threading.Thread(target = exampleftp2.envio_server_backup, args = (direccion,))
	thread_txt_bk.daemon = True
	thread_txt_bk.start()
	thread_txt_bk.join()
	#lock.release()
	print "Hilos are Done!"

run(setup,loop)














  	
	
   	
    	


