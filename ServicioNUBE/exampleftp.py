#usa M. Romero on 06/10/10.
# Script Description:
# 1) Deletes the existing folders AND files on FTP
# 2) Lists folders on rcant7 and creates the same list of folders on FTP
# 3) Transfers TIF images from rcant7 to the corresponding folders on FTP
# 4) Deletes the list of folders on rcant7
# Note: The "notes.txt" file on rcant7 isn't transferred or deleted.
# ---------------------------------------------------------------------------#

import ftplib, os, shutil, sys

startpath = r"/home/felipe/"
ftp = ftplib.FTP('ftp.academicosvirtuales.net')
ftp.login('academic', 'M@Xwell10291734')
directory = '/public_ftp/incoming/imagenesDETEK/'
ftp.cwd(directory)

print "Script: Image transfer from BBB  to FTP now executing..."
print " "

ftpdirset = ftp.nlst(directory)
for ftpd in ftpdirset:
    dirfold = directory + ftpd
    print "Visiting existing FTP folders/files: "  + dirfold
    try:
        ftp.delete(dirfold)
        print "FTP file deleted: " + dirfold
        print "-" * 70
    except:        
        ftpfiles = ftp.nlst(dirfold)
        for ftpfile in ftpfiles:
            dirfoldfile = dirfold + "/" + ftpfile
            print "     File in folder deleted: " + dirfoldfile
            ftp.delete(dirfoldfile)
        ftp.rmd(dirfold)
        print "FTP folder deleted: " + dirfold
        print "-" * 70
print "-" * 70

print "FTP directory " + directory + " has been emptied."
print "Images will be transferred."
print "-" * 70
print "-" * 70
print " "

dirListST = os.listdir(startpath)
for folder in dirListST:
    if os.path.isdir(startpath + "\\"+ folder):    
        print "Creating new " + folder + " folder in FTP " +  directory
        print "Transferring files to FTP now..."
        ftp.cwd(directory)
        ftp.mkd(folder)
        stpathfolder = os.path.join(startpath, folder)
        dirListST2 = os.listdir(stpathfolder)
        for specfile in dirListST2:                
            basename, extension = specfile.split(".")
            pathforspecfile = stpathfolder + "\\" + specfile
            ftp1 = startpath + "\\"+ folder + "\\" + specfile
            ftp.cwd(directory + folder + "/")
            if extension == "JPG" or extension == "txt":
                f = open(ftp1,"rb")
                a = "STOR " + specfile
                ftp.storbinary (a, f)
                print "     Transfer completed for " + specfile
                f.close()
            else:
                print "     File not transferred: " + specfile
        print stpathfolder + " has been deleted."
        shutil.rmtree(stpathfolder)
    else:
        print "*** " + folder + " is not a directory. Not transferred from BBB."
    print "-" * 70
print "-" * 70
print "IMAGE TRANSFER HAS BEEN COMPLETED."
                              
ftp.quit()
