# serial_echo.py - Alexander Hiam - 4/15/12
# 
# Prints all incoming data on Serial2 and echos it back.
# 
# Serial2 TX = pin 21 on P9 header
# Serial2 RX = pin 22 on P9 header
# 
# This example is in the public domain

from bbio import *
import threading

import exampleftp2
import File_Test
import time
import captura_img
import sys


data1 = 0
pos = 0
x = 0
aux = [] 
b = 0
j = 0
k=1

FLAG_archivo = True
Dif_Tem = 0
Cont = 0
Inicio = 0


i = 0
Recepcion = []
DatoXY = []

filename=''
File = ''
direccion = ''
	 
## Adicion de dato desde puerto serial
def addDato(Dato, auxCop):

     global x, b
     auxCop.insert(x,Dato.encode("hex"))
     x = x+1
     if(x == 19):
       x = 0	
       b = 1		
               
def idenTrama(trama):
     global Recepcion
     global j, k, i	
     print "len = ", len(trama)
     for i in range(len(trama)):
	  if str(trama[i])=='7e':
	     print "i -->", i
	     while(j < 19):	       	
	       Recepcion.insert(j,trama[(j+i)])
               j =j+1 
     #print "Dato ",k," : ", Recepcion
     k=k+1
     if (k==10):
 	k=0


def setup():
   #global filename
  # Start Serial2 at 9600 baud:
  Serial2.begin(9600)
 # lock=threading.lock()


def loop():
  
  global b , j , i	
  global aux
  global Recepcion
  global filename, File  , direccion
  global FLAG_archivo, Cont, captura_img, Inicio
  Hay_dato = False
  

  if (Serial2.available()):
	if (FLAG_archivo == True):
           name="datos"
	   hora=time.strftime("%a%d%b%Y_%H_%M_%S")
	   filename = name + hora + str('.txt')
	   carpeta = 'Prueba' + str(Cont)
	   direccion = File + '/' + filename  #directorio enviado para ingresar dato al TXT
	   
           File_Test.crearCarpeta(R'/media/6BFE-9C21/' + carpeta)
	   File = '/media/6BFE-9C21/' + carpeta
   	   File_Test.crearTXT(File, filename)
           FLAG_archivo= False
	   Cont +=1	
	

  #Serial2.write("a")
  if (Serial2.available()):
    Inicio=time.time()
    # There's incoming data
    Hay_dato = True

    while(Serial2.available()):
  
        data = Serial2.read()
	

       # data1 = data.encode("hex")
       	
	addDato(data,aux)

    if(b == 1):	
      idenTrama(aux)
      
      if (Recepcion == []):
          print "envia vacio\n" 
	  File_Test.grabarTXT(direccion,"0")
      else:
          print "enviando\n"	
	  File_Test.grabarTXT(direccion, Recepcion[17])
	  # COLOCAR FUNCION PARA ENVIAR DATO A WEB 
      aux=[]
      j = 0 

    b = 0	 	
    i = 0	
    Recepcion = []
    Trama = []
  Fin = time.time()
  Dif_Tem = Fin-Inicio
  Dif_Tem = int(Dif_Tem)
  if(Dif_Tem == 15):
        print "Entra Hilo"
 	FLAG_archivo = True
	#lock.acquire()

	# Hilo para captura de imagen
        thread = threading.Thread(target = captura_img.get_Image, args = (File,))
	thread = threading.Thread(target = captura_img.get_Image, args = (File,))
	thread.daemon = True
	thread.start()
	thread.join()

	"""# Hilo para subir archivos a ftp
	thread = threading.Thread(target = exampleftp2.envio_server, args = (carpeta,))
	thread.daemon = True
	thread.start()
	thread.join()
	#lock.release()"""
        print "Sale Hilo" 

	#captura_img.get_Image(File)
  #print "TIEMPO", Dif_Tem 	

    #if (b == 0):

    #Serial2.write(data)


run(setup,loop)














  	
	
   	
    	


