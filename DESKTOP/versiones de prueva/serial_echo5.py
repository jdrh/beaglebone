# serial_echo.py - Alexander Hiam - 4/15/12
# 
# Prints all incoming data on Serial2 and echos it back.
# 
# Serial2 TX = pin 21 on P9 header
# Serial2 RX = pin 22 on P9 header
# 
# This example is in the public domain

from bbio import *

import server
import File_Test
import time
#import captura_img


data1 = 0
pos = 0
x = 0
aux = [] 
b = 0
j = 0
k=1

i = 0
Recepcion = []
DatoXY = []

filename=''

## Adicion de dato desde puerto serial
def addDato(Dato, auxCop):

     global x, b
     auxCop.insert(x,Dato.encode("hex"))
     x = x+1
     if(x == 19):
       x = 0	
       b = 1		
               
def idenTrama(trama):
     global Recepcion
     global j, k, i	
     print "len = ", len(trama)
     for i in range(len(trama)):
	  if str(trama[i])=='7e':
	     print "i -->", i
	     while(j < 19):	       	
	       Recepcion.insert(j,trama[(j+i)])
               j =j+1 
     #print "Dato ",k," : ", Recepcion
     k=k+1
     if (k==10):
 	k=0
 

def setup():
   global filename
  # Start Serial2 at 9600 baud:
   Serial2.begin(9600)
   name="datos"
   hora=time.strftime("%a%d%b%Y_%H_%M_%S")
   filename = name + hora + str('.txt')
   File_Test.crearCarpeta(R'/media/6BFE-9C21/Prueba')
   File_Test.crearTXT('/media/6BFE-9C21/Prueba/', filename)
   #File_Test.crearTXT('/media/6BFE-9C21/Prueba/')


def loop():
  global b , j , i	
  global aux
  global Recepcion
  global filename

  #Serial2.write("a")
  if (Serial2.available()):

    # There's incoming data

    while(Serial2.available()):
      # If multiple characters are being sent we want to catch
      # them all, so add received byte to our data string and 
      # delay a little to give the next byte time to arrive:
        data = Serial2.read()
	

       # data1 = data.encode("hex")
       	
	addDato(data,aux)

    if(b == 1):	
      idenTrama(aux)
      if (Recepcion == []):
          print "envia vacio\n" 
	  #File_Test.grabarTXT('/media/6BFE-9C21/Prueba/',"0")
	  File_Test.grabarTXT('/media/6BFE-9C21/Prueba/', filename,"0")
      else:
          print "enviando\n"
          #File_Test.grabarTXT('/media/6BFE-9C21/Prueba/',Recepcion[17])
	  File_Test.grabarTXT('/media/6BFE-9C21/Prueba/', filename, Recepcion[17])
	  # COLOCAR FUNCION PARA ENVIAR DATO A WEB 
      aux=[]
      j = 0
 
    b = 0	 	
    i = 0	
    Recepcion = []
    Trama = []
    
    #Serial2.write(data)


run(setup,loop)














  	
	
   	
    	


