# serial_echo.py - Alexander Hiam - 4/15/12
# 
# Prints all incoming data on Serial2 and echos it back.
# 
# Serial2 TX = pin 21 on P9 header
# Serial2 RX = pin 22 on P9 header
# 
# This example is in the public domain

from bbio import *

import server

data1 = 0
pos = 0
x = 0
aux = [] 
b = 0
j = 0
k=1
i = 0
Recepcion = []
DatoXY = []


## Adicion de dato desde puerto serial
def addDato(Dato, auxCop):

     global x, b#, aux,
     auxCop.insert(x,Dato.encode("hex"))
     x = x+1
     if(x == 19):
       x = 0	
       b = 1		
     #print "Dato:\n " , auxCop
          
def idenTrama(trama):
     global Recepcion
     global j, k	
     for i in range(len(trama)):
	  if str(trama[i])=='7e':
	     while(j < 19):	       	
	       Recepcion.insert(j,trama[(j+i)])
               j =j+1
     print "Dato ",k," : ", Recepcion
     k=k+1
     if (k==10):
 	k=0
	
     	

def setup():
  # Start Serial2 at 9600 baud:
  Serial2.begin(19200)

def loop():
  global b , j , i	
  global aux
  global Recepcion

  #Serial2.write("a")
  if (Serial2.available()):

    # There's incoming data
    data = ''
    while(Serial2.available()):
      # If multiple characters are being sent we want to catch
      # them all, so add received byte to our data string and 
      # delay a little to give the next byte time to arrive:
        data = Serial2.read()
	

       # data1 = data.encode("hex")
       	
	addDato(data,aux)
    #print "Trama:\n " , aux

    if(b == 1):	
       idenTrama(aux)
       #Paquete = "".join(str(x) for x in Recepcion)	
       #print "Trama:\n " , Paquete 
       if(Recepcion == []):	
         server.envio_server(0)
       else:
         server.envio_server(Recepcion[17])
       aux=[]
       j = 0
    b = 0	
   	
    i = 0	
    Recepcion = []
    Trama = []
    
    #print "Trama:\n " , Recepcion  

    #Serial2.write(data)


run(setup,loop)














  	
	
   	
    	


