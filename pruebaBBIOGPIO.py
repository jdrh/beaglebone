# digitalRead.py - Alexander Hiam - 2/2012
# USR3 LED mirrors GPIO1_6 until CTRL-C is pressed.
#
# This example is in the public domain

from bbio import *


# Create a setup function:
def setup():
  # Set the GPIO pins:
  pinMode(USR3, OUTPUT)
  pinMode(GPIO1_7, OUTPUT)
  pinMode(GPIO1_6, INPUT)

# Create a main function:
def loop():
  state = digitalRead(GPIO1_6)
  print "estado ", state
  digitalWrite(USR3, state)
  digitalWrite(GPIO1_7, state)

  # It's good to put a bit of a delay in if possible
  # to keep the processor happy:
  delay(200)

# Start the loop:
run(setup, loop)
